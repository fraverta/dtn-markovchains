import sys
import operator
from functools import reduce
import copy

FORBID_RETURN_TO_SENDER = True


class Contact:
    # To generate unique identifier by each contact
    __identifier = 0

    def __init__(self, f, t, s_time, e_time, data_rate, work):
        self.source_eid = f
        self.destination_eid = t
        self.s_time = s_time
        self.e_time = e_time
        self.data_rate = data_rate
        self.work = work
        self.capacity = (
                        self.e_time - self.s_time) * self.data_rate  # The maximum capacity of a contact, it never changes.
        self.residual_volume = self.capacity  # The volume of contact which is still available

        self.id = Contact.__identifier
        Contact.__identifier += 1

    def __str__(self):
        # return "(f:%d,t:%d,sT%d,eT:%d,dR:%d)"%(self.f,self.t,self.sTime,self.eTime,self.dataRate)
        return "(f:%d,t:%d,rV:%d)" % (self.source_eid, self.destination_eid, self.residual_volume)

    __repr__ = __str__


class CP:
    # Contacts = {node_id(int):[Contacts]}
    def __init__(self, contacts):
        self.contacts = contacts

    def get_contact_by_id(self, id):
        for n in self.contacts.keys():
            for c in self.contacts[n]:
                if c.id == id:
                    return c

        print("Error in Contact:getContactById(): Contact Id = %d was not found" % (id))
        sys.exit()

    def get_contacts(self):
        return reduce(operator.concat, self.contacts.values(), [])

    def get_contacts_by_source(self, id):
        return self.contacts[id]

    def __str__(self):
        return "CP: " + str(self.contacts)

    __repr__ = __str__
class Route:
    def __init__(self, hops):
        self.hops = hops
        self.arrival_time = sys.maxsize
        self.from_time = sys.maxsize
        self.to_time = 0
        self.max_volume = 0

    def next_hop(self):
        return self.hops[0].destination_eid

    def next_hop_contact(self):
        return self.hops[0]
    def __str__(self):
        res = ""
        for c in self.hops:
            res += c.__str__() + "-> "
        res = res[0:len(res) - 3]
        return res


class Bundle:
    def __init__(self, f, t, byte_length, ts, sender_id=-1):
        # Bundle protocol fields (set by source node)
        self.source_eid = f
        self.destination_eid = t
        self.byte_length = byte_length
        self.creation_timestamp = ts
        self.ttl = sys.maxsize

        self.hop_count = 0
        self.sender_eid = sender_id
        self.current_node = f

    def __str__(self):
        return "Bundle(f=%d,t=%d,current=%d,sender_id=%d)" % (self.source_eid, self.destination_eid,
                                                              self.current_node ,self.sender_eid)


    __repr__ = __str__

'''
cp: ContactPlan
rootContact: Contact
terminusNode: Int
return Route
'''


def find_next_best_route_by_bdt(cp, rootContact, terminusNode):
    current_contact = rootContact
    final_contact = None
    earliest_final_arrival_time = sys.maxsize
    while True:
        current_neighbors = cp.get_contacts_by_source(current_contact.destination_eid)
        for neighbord in current_neighbors:

            # Fist check if it has to be considered
            if neighbord.e_time <= current_contact.work['arrival_time']:
                continue  # ignore contact

            # This contact is suppressed/visited, ignore it.
            if neighbord.work['suppressed'] or neighbord.work['visited']:
                continue

            # compute capacity TODO: Ojo con esto, revisar! Posible fuente de errores.
            if neighbord.work['capacity'] == 0:
                # In DTNSim, we have the following line but since my point of view it never change(I mean, it isn't updated)
                # neighbord.work['capacity'] = neighbord.getDataRate() * neighbord.getDuration()
                neighbord.work['capacity'] = neighbord.capacity

            # calculate the cost of this contact
            if neighbord.s_time < current_contact.work['arrival_time']:
                arrival_time = current_contact.work['arrival_time']
            else:
                arrival_time = neighbord.s_time

            # update the cost for this contact
            if arrival_time < neighbord.work['arrival_time']:
                neighbord.work['arrival_time'] = arrival_time
                neighbord.work['predecessor'] = current_contact

                # if this contact reaches the terminusNode consider it as final contact
                if neighbord.destination_eid == terminusNode:
                    if neighbord.work['arrival_time'] < earliest_final_arrival_time:
                        earliest_final_arrival_time = neighbord.work['arrival_time']
                        final_contact = neighbord

        # end for current_contact neighbors
        current_contact.work['visited'] = True
        # select next (best) contact to move to in next iteration
        next_contact = None
        earliest_arrival_time = sys.maxsize
        for contact in cp.get_contacts():
            # Do not evaluate suppressed or visited contacts
            if contact.work['suppressed'] or contact.work['visited']:
                continue

            # if the arrival time is worst than the best found so far, ignore
            if contact.work['arrival_time'] > earliest_final_arrival_time:
                continue

            # Then this might be the best candidate contact
            if contact.work['arrival_time'] < earliest_arrival_time:
                next_contact = contact
                earliest_arrival_time = contact.work['arrival_time']

        # end for select next best contact
        if next_contact is None:
            break  # No next contact exit searach

        # update next contact and go with the next iteration
        current_contact = next_contact

    # end While.
    if final_contact is not None:
        route = Route([])
        route.arrival_time = earliest_final_arrival_time
        earliest_end_time = sys.maxsize
        max_capacity = sys.maxsize

        contact = final_contact
        while contact != rootContact:

            # get earliest end time
            if contact.e_time < earliest_end_time:
                earliest_end_time = contact.e_time

            # get the minimal capacity
            if contact.capacity < max_capacity:
                max_capacity = contact.capacity

            route.hops.insert(0, contact)
            contact = contact.work['predecessor']

        route.from_time = route.hops[0].s_time
        route.to_time = earliest_end_time
        route.max_volume = max_capacity  # the least volume of route links
        return route
    else:
        return None


'''
sourceNode: int
terminusNode: int
simTime: double
'''


def load_route_list(cp, source_node, terminus_node, sim_time, route_list):
    root_contact = Contact(source_node, source_node, 0, 0, 0, {'arrival_time': sim_time})
    for c in cp.get_contacts():
        c.work = {'arrival_time': sys.maxsize, 'capacity': 0, 'predecessor': None, 'visited': False,
                  'suppressed': False}
        anchor_contact = None
    while True:
        route = find_next_best_route_by_bdt(cp, root_contact, terminus_node)
        if route is None:
            break
        first_contact = route.hops[0]
        if anchor_contact is not None:
            if anchor_contact != first_contact:
                for contact in cp.getContacts():
                    contact.work['arrival_time'] = sys.maxsize
                    contact.work['predecessor'] = None
                    contact.work['visited'] = False
                    if contact.source_eid != source_node:
                        contact.work['suppressed'] = False

                anchor_contact.work['suppressed'] = True
                anchor_contact = None
                continue

        route_list[terminus_node].append(route)
        if route.to_time == first_contact.e_time:
            limit_contact = first_contact
        else:
            anchor_contact = first_contact
            for contact in route.hops:
                if contact.end_t == route.to_time:
                    limit_contact = contact
                    break
        limit_contact.work['suppressed'] = True
        for contact in cp.get_contacts():
            contact.work['arrival_time'] = sys.maxsize
            contact.work['predecessor'] = None
            contact.work['visited'] = False


'''
    bundle: Bundle
    simTime: Double
    excludedNodes:[int]
    Rl: {int:[Route]}

    Pn:{int(denotes node id): {'arrival_time':double, 'hop_count': int,
                               'forfeit_time':double,route}}
'''


def identify_proximate_nodes(cp, bundle, sim_time, excluded_nodes, route_list):
    if bundle.destination_eid not in route_list.keys():
        route_list[bundle.destination_eid] = []
        load_route_list(cp, bundle.current_node, bundle.destination_eid, sim_time, route_list)
    proximate_nodes = {}
    for route in route_list[bundle.destination_eid]:
        # --- Discard Route ---
        if route.to_time <= sim_time:
            continue  # ignore past route
        if route.arrival_time > bundle.ttl:
            continue  # route arrives too late
        #        if bundle.getByteLenght() > route.capacity:
        #            continue #not enought capacity
        if bundle.byte_length > route.hops[0].residual_volume:
            continue  # if bundle does not fit in first contact ignore it.
        if route.next_hop() in excluded_nodes:
            continue  # next hop is excluded

        # --- Check if it is the best for the neighbor ---
        if route.next_hop() in proximate_nodes.keys():
            proximate_node = proximate_nodes[route.next_hop()]
            if route.arrival_time < proximate_node['arrival_time']:
                proximate_node['arrival_time'] = route.arrival_time
                proximate_node['hop_count'] = len(route.hops)
                proximate_node['forfeit_ime'] = route.to_time
                proximate_node['route'] = route

            elif route.arrival_time > proximate_node['arrival_time']:
                pass

            elif len(route.hops) < proximate_node['hop_count']:
                proximate_node['arrival_time'] = route.arrivalTime
                proximate_node['hop_count'] = len(route.hops)
                proximate_node['forfeit_time'] = route.toTime
                proximate_node['route'] = route
            elif len(route.hops) > proximate_node['hop_count']:
                pass

        else:
            proximate_nodes[route.next_hop()] = {'arrival_time': route.arrival_time, 'hop_count': len(route.hops),
                                                 'forfeit_time': route.to_time, 'route': route}

    return proximate_nodes


def cgrForward(cp, bundle, sim_time):
    # En means excluded nodes
    Rl = {}
    En = []
    if FORBID_RETURN_TO_SENDER:
        En.append(bundle.current_node)
    Pn = identify_proximate_nodes(cp, bundle, sim_time, En, Rl)
    next_hop = None
    for pn in Pn:
        if next_hop is None:
            next_hop = pn
        elif Pn[pn]['arrival_time'] < Pn[next_hop]['arrival_time']:
            next_hop = pn
        elif Pn[pn]['arrival_time'] > Pn[next_hop]['arrival_time']:
            continue
        elif Pn[pn]['hop_count'] < Pn[next_hop]['hop_count']:
            next_hop = pn
        elif Pn[pn]['hop_count'] > Pn[next_hop]['hop_count']:
            continue
        elif Pn[pn]['route'].hops[0].source_eid < Pn[next_hop]['route'].hops[0].source_eid:
            next_hop = pn

    if next_hop is not None:
        return Pn[next_hop]['route']
    else:
        return None
    '''
    if nextHop is not None:
        print("Route to " + str(nextHop) + ": " + str(Pn[nextHop]) )
    else:
        print("No route is available")
    '''


def main():
    rootContact = Contact(0, 0, 0, sys.maxsize, 1,
                          {'capacity': 0, 'arrival_time': 0, 'predecessor': None, 'visited': False,
                           'suppressed': False})
    c0 = Contact(0, 1, 0, 1, 1, {'capacity': 0, 'arrival_time': sys.maxsize, 'predecessor': None, 'visited': False,
                                 'suppressed': False})
    c1 = Contact(1, 2, 1, 2, 1, {'capacity': 0, 'arrival_time': sys.maxsize, 'predecessor': None, 'visited': False,
                                 'suppressed': False})
    c2 = Contact(0, 2, 2, 3, 1, {'capacity': 0, 'arrival_time': sys.maxsize, 'predecessor': None, 'visited': False,
                                 'suppressed': False})
    cp = CP({0: [c0, c2], 1: [c1], 2: []})
    bundle = Bundle(0, 2, 1, 0)
    # Rl={2:[]}
    # loadRouteList(cp, 0, 2, 0, Rl)
    # print(Rl.__str__())
    # print(identifyProximateNodes(cp, bundle, 0, [], {}))


    print(cgrForward(cp, bundle, 0))
    cp.contacts[0][0].residual_volume = 0
    print(cgrForward(cp, bundle, 0))

    # c = copy.deepcopy(cp)
    # c.contacts[0][0].residualVolume = 1
    # cp.contacts[0][0].residualVolume = 10
    # print(cp.contacts[0][0].residualVolume)
    # print(c.contacts[0][0].residualVolume)


#    print(findNextBestRouteByBDT(cp, rootContact, 2))
#    print(findNextBestRouteByHops(cp, rootContact, 2))




